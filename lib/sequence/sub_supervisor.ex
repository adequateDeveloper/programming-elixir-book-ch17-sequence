defmodule Sequence.SubSupervisor do
  use Supervisor

  # API

  def start_link(stash_pid) do
    {:ok, _pid} = Supervisor.start_link(__MODULE__, stash_pid)
  end


  # Callbacks

  # Start the worker process Server passing it the Stash pid
  # Then supervise the child processess with a one_for_one restart strategy
  def init(stash_pid) do
    child_processes = [ worker(Sequence.Server, [stash_pid]) ]
    supervise child_processes, strategy: :one_for_one
  end

end