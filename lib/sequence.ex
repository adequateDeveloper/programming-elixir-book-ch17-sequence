defmodule Sequence do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    {:ok, _pid} = Sequence.Supervisor.start_link(123)

#    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
#    children = [
      # Starts a worker by calling: Sequence.Worker.start_link(arg1, arg2, arg3)
#       worker(Sequence.Server, [123])
#    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    # On starting, the Supervisor will call start_link for each of the managed child processes
#    opts = [strategy: :one_for_one, name: Sequence.Supervisor]
#    {:ok, _pid} = Supervisor.start_link(children, opts)
  end
end
